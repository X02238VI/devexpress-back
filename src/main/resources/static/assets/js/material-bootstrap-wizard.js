/*!

 =========================================================
 * Material Bootstrap Wizard - v1.0.2
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/material-bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/material-bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Material Bootstrap Wizard Functions
var vozInstrucciones = "Recuerda: este sistema es solo para compras realizadas con tarjeta el corteingl&iacutes. Lea atentamente las instrucciones";
var vozDatos = "Estos datos ser&aacuten utilizados con el &uacutenico fin de notificarle el estado de la devoluci&oacuten.";
var vozEscanear = "Recuerda escanear el recibo que corresponde al producto que desea devolver";
var vozGenerar = "Recuerda introducir la copia de su recibo que se ha impreso el la bolsa de devoluci&oacuten";
var vozGracias = "Muchas gracias, se realizar&aacute el abono lo antes posible, y recibir&aacute una comunicaci&oacuten del mismo en su correo electr&oacutenico";

var tiempoEspera;

var searchVisible = 0;
var transparent = true;
var mobile_device = false;
var IP;

var codOperacion=0;


$(document).ready(function(){

    $.material.init();

    
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    /* capturamos la IP*/
    getIP().then (result =>{
        IP = result;
    });

    // Code for the Validator
    var $validator = $('.wizard-card form').validate({
          rules: {
            firstname: {
              required: true,
              minlength: 3
            },
            lastname: {
              required: true,
              minlength: 3
            },
            email: {
              required: true,
              minlength: 3,
            },
            telefono: {
              required: true,
              minlength: 9,
              maxlength: 9,
            },
            barcodeValue:{
                required: true,
                maxlength: 23,
                minlength: 23
            }
        },

        errorPlacement: function(error, element) {
            $(element).parent('div').addClass('has-error');
         }
    });

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function(tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }

            /*switch (index){
                case 1:
                    locucion(vozDatos);
                    break;
                case 2:
                    locucion(vozEscanear);
                    $('barcodeValue').focus();
                    $('barcodeValue').select();
                    console.log('switch onnext');
                    break;
                default:
                    break;
            }*/
        },

        onInit : function(tab, navigation, index){
            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $wizard = navigation.closest('.wizard-card');

            $first_li = navigation.find('li:first-child a').html();
            $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
            $('.wizard-card .wizard-navigation').append($moving_div);

            refreshAnimation($wizard, index);

            $('.moving-tab').css('transition','transform 0s');
       },

        onTabClick : function(tab, navigation, index){
            var $valid = $('.wizard-card form').valid();

            return false;

            /*if(!$valid){
                return false;
            } */
        },

        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;

            var $wizard = navigation.closest('.wizard-card');

            switch (index){
                    case 0:                 //Instrucciones
                        $('#exit').show(); 
                        document.getElementById('next').value = "Comenzar";
                        //tiempoEspera =  window.setTimeout(terminar,30000);
                        break;
                    case 1:                 //Introducir código
                        document.getElementById('next').value = "Siguiente";
                        locucion(vozEscanear);
                        document.getElementById('barcodeValue').focus();
                        $('#exit').show();
                        window.clearTimeout(tiempoEspera);
                        tiempoEspera =  window.setTimeout(terminar,300000);
                        break;
                    case 2:                 //Introducir datos
                        locucion(vozDatos);
                        document.getElementById('email').focus();
                        $('#exit').show();
                        console.log('next hide');
                        //document.getElementById('next').style.display='none';
                        window.clearTimeout(tiempoEspera);
                        tiempoEspera =  window.setTimeout(terminar,300000);
                        console.log('switch ontabshow');
                        break;
                    case 3:                 //Imprimir codigo
                        var email = document.getElementById("email").value;
                        var tfo = document.getElementById("telefono").value;
                        var codigo = document.getElementById("barcodeValue").value;
                        var codOperacion = document.getElementById("codOperacion").value;
                        callServicePOST(email, tfo, codigo, codOperacion).then(estado => {
                            console.log ('Resp en prueba:',estado)
                            if (estado){
                               console.log('OK srv POST');
                               $('#exit').hide();
                                locucion(vozGenerar);
                                locucion(vozGracias);
                                imprimirDosTickets();

                                window.setTimeout(popupGracias,20000);
                                
                                window.clearTimeout(tiempoEspera);
                                tiempoEspera =  window.setTimeout(terminar,50000);
                            }
                            else
                            {
                                console.log('KO srv POST');
                                bootbox.alert({ size: "small",
                                            title: "Error de validacion", 
                                            message: "Lo sentimos, el ticket escaneado no corresponde a una compra con tarjeta ECI. Acuda al SAC para tramitar su devoluci&oacuten", 
                                            buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                            callback: function(){ 
                                                        terminar();
                                                        } });
                            }
                        });

                    default:
                        break;
                }
        
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            button_text = navigation.find('li:nth-child(' + $current + ') a').html();

            setTimeout(function(){
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if( !index == 0 ){
                $(checkbox).css({
                    'opacity':'0',
                    'visibility':'hidden',
                    'position':'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity':'1',
                    'visibility':'visible'
                });
            }

            refreshAnimation($wizard, index);
        }
    });


    // Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function(){
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked','true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function(){
        if( $(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked','true');
        }
    });

    $('.set-full-height').css('height', 'auto');

/* Captura de eventos para mostrar teclado */ 
$('.hiddenInputCodigo').click(function(){
        var codigo = document.getElementById("barcodeValue").value;
        document.getElementById("hiddenCodigo").value = codigo;
        $('#hiddenCodigo').data('keyboard').reveal();
        return false;
    });

$('.hiddenInputEmail').click(function(){
        $('#hiddenEmail').data('keyboard').reveal();
        //$('#next').show();
        return false;
    });

$('.hiddenInputTfo').click(function(){
        $('#hiddenTfo').data('keyboard').reveal();
        return false;
    });

// Initialize keyboard script on hidden input
// set "position.of" to the same link as above
$('#hiddenCodigo').keyboard({
    layout: 'qwerty',
    position     : {
        of : $('.hiddenInputCodigo'),
        my : 'right top',
        at : 'right top'
    },
    layout: 'qwerty',
    display: {
        'bksp'   :  "\u2190",
        'accept' : 'Aceptar',
        'normal' : 'ABC',
        },

    layout: 'custom',
        customLayout: {
            'normal': [
                '1 2 3 4 5 6 7 8 9 0 {bksp}',
                'Q W E R T Y U I O P',
                'A S D F G H J K L',
                'Z X C V B N M',
                ' {accept}'
            ]
        }
        
});

$('#hiddenEmail').keyboard({
    layout: 'qwerty',
    position     : {
        of : $('.hiddenInputEmail'),
        my : 'right top',
        at : 'right top',
    },
    layout: 'qwerty',
    display: {
        'bksp'   :  "\u2190",
        'accept' : 'Aceptar',
        'normal' : 'ABC',
        },

    layout: 'custom',
        customLayout: {
            'normal': [
                '1 2 3 4 5 6 7 8 9 0 {bksp}',
                'q w e r t y u i o p',
                'a s d f g h j k l',
                'z x c v b n m _ -',
                ' @ {accept} .'
            ]
        }
        
});

$('#hiddenTfo').keyboard({
    layout: 'qwerty',
    position     : {
        of : $('.hiddenInputTfo'),
        my : 'right top',
        at : 'right top'
    },
    display: {
        'bksp'   :  "\u2190",
        'accept' : 'Aceptar',
        'normal' : 'ABC'
        
    },

    layout: 'custom',
    customLayout: {
        'normal': [
        '1 2 3',
        '4 5 6', 
        '7 8 9' ,
        ' 0 {bksp}',
        '{accept}'
    ]
    }
    });
});

//Function to show image before upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(window).resize(function(){
    $('.wizard-card').each(function(){
        $wizard = $(this);

        index = $wizard.bootstrapWizard('currentIndex');
        refreshAnimation($wizard, index);

        $('.moving-tab').css({
            'transition': 'transform 0s'
        });
    });
});

function refreshAnimation($wizard, index){
    $total = $wizard.find('.nav li').length;
    $li_width = 100/$total;

    total_steps = $wizard.find('.nav li').length;
    move_distance = $wizard.width() / total_steps;
    index_temp = index;
    vertical_level = 0;

    mobile_device = $(document).width() < 600 && $total > 3;

    if(mobile_device){
        move_distance = $wizard.width() / 2;
        index_temp = index % 2;
        $li_width = 50;
    }

    $wizard.find('.nav li').css('width',$li_width + '%');

    step_width = move_distance;
    move_distance = move_distance * index_temp;

    $current = index + 1;

    if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
        move_distance -= 8;
    } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
        move_distance += 8;
    }

    if(mobile_device){
        vertical_level = parseInt(index / 2);
        vertical_level = vertical_level * 38;
    }

    $wizard.find('.moving-tab').css('width', step_width);
    $('.moving-tab').css({
        'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

    });
}

materialDesign = {

    checkScrollForTransparentNavbar: debounce(function() {
                if($(document).scrollTop() > 260 ) {
                    if(transparent) {
                        transparent = false;
                        $('.navbar-color-on-scroll').removeClass('navbar-transparent');
                    }
                } else {
                    if( !transparent ) {
                        transparent = true;
                        $('.navbar-color-on-scroll').addClass('navbar-transparent');
                    }
                }
        }, 17)

}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
};

/******** Funciones para wizard.html ***********/



function locucion(texto){
  
  var speechSynthesisUtterance = new SpeechSynthesisUtterance(texto); 
  window.speechSynthesis.speak(speechSynthesisUtterance);
}

function popupGracias(){
    $('#msgFinal').text("");
    bootbox.alert({ size: "small",
        title: "", 
        message: "Muchas gracias, se realizar&aacute el abono lo antes posible y recibir&aacute una comunicaci&oacuten del mismo en su correo electr&oacutenico", 
        buttons: { ok: { label: 'OK', className: 'btn-success' }}
        /*callback: function(){ 
                    terminar();
                    }*/
         });
    window.setTimeout(terminar,10000);

}

function imprimirDosTickets(){
  var email = document.getElementById("tkEmail").value;  
  var cod = document.getElementById("barcodeValue").value;
  var tfo = document.getElementById("tkTelefono").value;
  //var codOperacion = 0;
  var codOperacion = document.getElementById("codOperacion").value;
 
var win = window.open('dosTickets?barcode=' + cod + '&email=' + email + '&tfo=' + tfo + '&codOperacion=' + codOperacion, 
                'Impresión de ticket', 'height=1,width=1');
this.window.focus();
 
}

function imprimirUnTicket(){
  var email = document.getElementById("tkEmail").value;  
  var cod = document.getElementById("barcodeValue").value;
  var tfo = document.getElementById("tkTelefono").value;
  //var codOperacion = 0;
  var codOperacion = document.getElementById("codOperacion").value;


var win = window.open('unTicket?barcode=' + cod + '&email=' + email + '&tfo=' + tfo + '&codOperacion=' + codOperacion, 
                'Impresión de ticket', 'height=1,width=1');
this.window.focus();
 
}

function generarCodigo(){

    fechaHora();
    var codigo = document.getElementById("barcodeValue").value; 

    $('#barcode').barcode(codigo, "code93",{fontSize:14});



    callServiceGET(codigo).then(result => {
        console.log ('Resp en generar codigo:',result)
        if (result){

            $('.btn-next').click();
        }
        else
        {
            
            /*bootbox.alert({ size: "small",
                        title: "Error de validacion", 
                        message: "Lo sentimos, el tique escaneado no es válido.", 
                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                        callback: function(){ 
                                    terminar();
                                    } });*/
        }
        
    });

}




function comprobarEmail(){
  var email = document.getElementById("email").value;
  var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  
  if(!regex.test(email)){
    
    
    bootbox.alert({ size: "small",
                    title: "Error en la entrada de datos", 
                    message: "El formato de email, no es correcto", 
                    buttons: { ok: { label: 'OK', className: 'btn-success' }},
                    callback: function(){ 
                                var email = document.getElementById("email");
                                email.value="";
                                } });

    return false;
  }else{
    return true;
  }
}

function copiarCodigo()
{
    var codigo = document.getElementById("hiddenCodigo").value;
    document.getElementById("barcodeValue").value = codigo;
            //$($wizard).find('.btn-next').click();
    $('.btn-next').click();

}

function copiarEmail()
{
    var email = document.getElementById("hiddenEmail").value;
    document.getElementById("email").value = email;
    if (comprobarEmail()){
        document.getElementById("tkEmail").value = email;
  }
}

function copiarTfo()
{
    var tfo = document.getElementById("hiddenTfo").value;
    document.getElementById("telefono").value = tfo;
    if (comprobarTelefono()){
        document.getElementById("tkTelefono").value = "Telefono:" + tfo;
    }
}


function comprobarTelefono(){
  var telefono = document.getElementById("telefono").value;
  if (telefono.length!=9){
    
    bootbox.alert({ size: "small",
                    title: "Error en la entrada de datos", 
                    message: "El tel&eacutefono debe tener 9 d&iacutegitos", 
                    buttons: { ok: { label: 'OK', className: 'btn-success' }},
                    callback: function(){ document.getElementById("telefono").value="";} });
    
    return false;
  }
  if (isNaN(telefono)){
    
    bootbox.alert({ size: "small",
                    title: "Error en la entrada de datos", 
                    message: "El tel&eacutefono solo debe contener n&uacutemeros", 
                    buttons: { ok: { label: 'OK', className: 'btn-success' }},
                    callback: function(){ document.getElementById("telefono").value=""; } });
    
    return false;
  }
  return true;
}


function VozInstrucciones(){
  console.log('cargarinstrucciones');
  locucion(vozInstrucciones);

  
   

}

function terminar(){
  
  document.getElementById("email").value="";
  document.getElementById("telefono").value="";
  document.getElementById("barcodeValue").value="";
  document.getElementById("codOperacion").value="";

  window.location="index";
  
  
}

function foco(caja){
  console.log('foco');
  document.getElementById('barcodeValue').focus();
}



function fechaHora(){
    var fecha = new Date();

    document.getElementById("fechaHora").value = "Fecha: " + fecha.getDate() + "/" + (fecha.getMonth()+1) + "/" + fecha.getFullYear() +
                        " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    //document.getElementById("fechaHora_").value = "Fecha: " + fecha.getDay() + "/" + fecha.getMonth() + "/" + fecha.getFullYear() + 
    //                    " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
}

function fechaHoraDosTickets(){
    var fecha = new Date();

    document.getElementById("fechaHora_").value = "Fecha: " + fecha.getDate() + "/" + (fecha.getMonth()+1) + "/" + fecha.getFullYear() +
                        " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    document.getElementById("fechaHora").value ="Fecha: " + fecha.getDate() + "/" + (fecha.getMonth()+1) + "/" + fecha.getFullYear() +
                        " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();                    
}

function fechaHoraUnTicket(){
    var fecha = new Date();

    document.getElementById("fechaHora").value ="Fecha: " + fecha.getDate() + "/" + (fecha.getMonth()+1) + "/" + fecha.getFullYear() +
                        " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();                    
}

/*const checkServerWithSugar = async (url) => {
  const response = await fetch(url);
  return `Estado del Servidor: ${response.status === 200 ? "OK" : "NOT OK"}`;
}


*/
const callServicePOST = async (email, tfo, codigo, codOperacion) => {
    var strUrl = urlHTML + "data/" + codigo;;
    var strBody;
    
     //'{"ip":"' + r + '"}';
        strBody = '{"ip":"' + IP +
              '", "email":"' + email +
              '", "number":"' + tfo + 
              '", "codOperacion":"' + codOperacion + 
              '"}';
                 
        console.log('body:', strBody);
        const response = await fetch(strUrl, {
              method: 'POST', 
              body: strBody, 
              headers:{
                'Content-Type': 'application/json'
              }
            }).then(res => {
                //res.json();
                console.log('res del fetch post:',res);
                if (res.status == 200){
                        resp=true;
                    }else{
                        console.log('otro');
                        bootbox.alert({ size: "small",
                                        title: "Error de validaci&oacuten", 
                                        message: "Lo sentimos, el tique escaneado no corresponde a una compra con tarjeta ECI. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                        callback: function(){ 
                                                    terminar();
                                                    } });
                        resp=false;
                    }
                    console.log('resp en fetch:', resp);
            })
            .catch(error => console.error('Error:', error))
            .then(datos => console.log('Success:', datos));

        console.log('Resp post:', resp);    
        return resp;
        
    
}

const callServiceGET = async (codigo) => {
    var strUrl;
    var resp;
    var respCode;
    var respDesc;
    var respErrCode;
    
   strUrl = urlHTML + "barcode/" + codigo;
   // strUrl = 'https://8913929f-eafa-4c31-aafa-c5bde9e173c0.mock.pstmn.io/dev-express/data/' + codigo;
   
   //strUrl = 'https://8913929f-eafa-4c31-aafa-c5bde9e173c0.mock.pstmn.io/dev-express/data/' + codigo;
console.log('strURL:',strUrl);
    const response = await fetch(strUrl)
        .then(function(response) {
            console.log('response.body =', response.body);
            console.log('response.bodyUsed =', response.bodyUsed);
            console.log('response.headers =', response.headers);
            console.log('response.ok =', response.ok);
            console.log('response.status =', response.status);
            console.log('response.statusText =', response.statusText);
            console.log('response.type =', response.type);
            console.log('response.url =', response.url);
            
            var body = Promise.resolve(response.json());
            body.then(function(value){
                respErrCode = value.errorId;
                respDesc = value.message;
                console.log('error_id:',value.errorId, 'desc:',respDesc);

                respCode=response.status;
                
                if (respCode == 200){
                    if (respErrCode == '401'){
                        console.log('401');
                        bootbox.alert({ size: "small",
                                        title: "Error de validaci&oacuten", 
                                        message: "Lo sentimos, el tique escaneado no corresponde a una compra con tarjeta ECI. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                        callback: function(){ 
                                                    terminar();
                                                    } });
                        
                        resp=false;
                    }else if(respErrCode == '402'){ //0XI5IS9XA8EQ098ACZ0NYNN
                        console.log('402');
                        bootbox.alert({ size: "small",
                                        title: "Error de validaci&oacuten", 
                                        message: "Lo sentimos, el tique escaneado no es v&aacutelido. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                        callback: function(){ 
                                                    terminar();
                                                    } });
                        resp=false;
                    }else if(respErrCode == '403'){ //0XI5IS9XA8EQ098ACZ0NYNN
                        console.log('403');
                        bootbox.alert({ size: "small",
                                        title: "Error de validaci&oacuten", 
                                        message: "Lo sentimos, No puede ser un ticket regalo. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                        callback: function(){ 
                                                    terminar();
                                                    } });
                        resp=false;
                    }else if(respErrCode == '400'){ //0XI5IS9XA8EQ098ACZ0NYNN
                        console.log('400');
                        bootbox.alert({ size: "small",
                                        title: "Error de validaci&oacuten", 
                                        message: "Lo sentimos, No puede ser un ticket regalo. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                        buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                        callback: function(){ 
                                                    terminar();
                                                    } });
                        resp=false;
                    }else{
                       resp=true;
                       console.log('resp en fetch:', resp);
                       codOperacion = respDesc;
                       document.getElementById("codOperacion").value = respDesc;
                    }
                }else{
                    console.log('otro');
                    bootbox.alert({ size: "small",
                                    title: "Error de validaci&oacuten", 
                                    message: "Lo sentimos, No puede ser un ticket regalo. <br>Dir&iacutejase a un vendedor para tramitar su devoluci&oacuten", 
                                    buttons: { ok: { label: 'OK', className: 'btn-success' }},
                                    callback: function(){ 
                                                terminar();
                                                } });
                    resp=false;
                }
               });
            
            document.getElementById("codOperacion").value = respDesc;
            resp=true;

            })
        /*.then(function(data) {
                console.log('data = ', data);
            })
        */    
        .catch(function(err) {
            console.error('err=',err);
            resp=false;
        });

        console.log('resp en callservice:',resp);

        return resp;
}
/******** PARA SOLOTICKET.HTML *****************/

function recibirParametrosDosTickets(){
  var paramstr = window.location.search.substr(1);
  var paramarr = paramstr.split ("&");
  var params = {};
  
fechaHoraDosTickets();

  for ( var i = 0; i < paramarr.length; i++) {
      var tmparr = paramarr[i].split("=");
      params[tmparr[0]] = tmparr[1];
  }

  var email = params['email'];
  var opSeparado = params['codOperacion'].substr(0,15);
  opSeparado = opSeparado.substr(0,3) + " / " + opSeparado.substr(3,4)  + " / " + opSeparado.substr(7,8);
    email1 = email.substr(0,33);
    email2 = email.substr(33,60);

  document.getElementById("tkEmail").value = "Email: " + email1;
  document.getElementById("tkEmail2").value = email2;
  document.getElementById("tkTelefono").value = params['tfo'];
  document.getElementById("codOperacion").value = opSeparado;
  document.getElementById("fecha").value = "Fecha: " + params['codOperacion'].substr(21,2) + "/" + params['codOperacion'].substr(19,2) + "/" + params['codOperacion'].substr(15,4);
  

  document.getElementById("tkEmail_").value = "Email: " + email1;
  document.getElementById("tkEmail2_").value = email2;
  document.getElementById("tkTelefono_").value = params['tfo'];
  document.getElementById("codOperacion_").value = opSeparado;
  document.getElementById("fecha_").value = "Fecha: " + params['codOperacion'].substr(21,2) + "/" + params['codOperacion'].substr(19,2) + "/" + params['codOperacion'].substr(15,4);
  

  
  $('#barcode_').barcode(params['barcode'], "code93",{fontSize:14});
  $('#barcode').barcode(params['barcode'], "code93",{fontSize:14});
  // , {barWidth:0.5, barHeight:14, fontSize:12}
  //JsBarcode("#barcode", params['barcode'], {width: 0.7, height:60, fontSize: 8, format:"CODE39"});
  //QR 1
  var qrcode = new QRCode("barcodeqr", {
    width: 50,
    height: 50,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});
    qrcode.clear(); // clear the code.
    qrcode.makeCode(params['barcode']);
    
    //QR2
    var qrcode2 = new QRCode("barcodeqr_", {
    width: 50,
    height: 50,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});
    qrcode2.clear(); // clear the code.
    qrcode2.makeCode(params['barcode']);

    window.print(0);
    sleep(1500);
    window.print(1);  
  
    window.close();
  

   
}

function recibirParametrosUnTicket(){
  var paramstr = window.location.search.substr(1);
  var paramarr = paramstr.split ("&");
  var params = {};
  
fechaHoraUnTicket();

  for ( var i = 0; i < paramarr.length; i++) {
      var tmparr = paramarr[i].split("=");
      params[tmparr[0]] = tmparr[1];
  }

  var email = params['email'];
  var opSeparado = params['codOperacion'].substr(0,15);
  opSeparado = opSeparado.substr(0,3) + " / " + opSeparado.substr(3,4)  + " / " + opSeparado.substr(7,8);
  
    email1 = email.substr(0,27);
    email2 = email.substr(27,60);

  document.getElementById("tkEmail").value = email1;
  document.getElementById("tkEmail2").value = email2;
  document.getElementById("tkTelefono").value = params['tfo'];
  document.getElementById("codOperacion").value = opSeparado;
  document.getElementById("fecha").value = "Fecha: " + params['codOperacion'].substr(21,2) + "/" + params['codOperacion'].substr(19,2) + "/" + params['codOperacion'].substr(15,4);
  
  $('#barcode').barcode(params['barcode'], "code93",{fontSize:14});
  
  var qrcode = new QRCode("barcodeqr", {
    width: 50,
    height: 50,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});
    qrcode.clear(); // clear the code.
    qrcode.makeCode(params['barcode']);
    
    window.print();
    
    window.close();
  

   
}

/************ otras *******/

function aleatorio(min, max){
    var ret = Math.floor(Math.random() * (max - min) + min); 
    console.log('random:', ret);
    return ret;
}

async function getIP(){
var ip;
    
       const ips = await $.getJSON("http://jsonip.com/?callback=?") 
        return ips.ip;
}



function prueba(){
    
   var fecha = new Date();

    var f = "Fecha: " + fecha.getDate() + "/" + (fecha.getMonth()+1) + "/" + fecha.getFullYear() + 
                        " Hora: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
   console.log(f);
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

