package com.maw.emd;

public class ExcepcionReturnsExpress extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	public ExcepcionReturnsExpress(String msg) {
        super(msg);
    }
	
}
