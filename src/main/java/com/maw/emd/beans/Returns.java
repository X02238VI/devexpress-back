package com.maw.emd.beans;

public class Returns {
	
	private String errorId;
	private String message;
	
	public Returns(String errorId, String message) {
		super();
		this.errorId = errorId;
		this.message = message;
	}

	
	public String getMessage() {
		return message;
	}
		
	
	public String getErrorId() {
		return errorId;
	}
		
	
	public void setMessage(String message) {
		this.message = message;
	}
		
	
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
}
