package com.maw.emd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication(scanBasePackages = {"com.maw.emd.dao","com.maw.emd.service","com.maw.emd.controllers"})
public class DevexBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevexBackApplication.class, args);
	}
}
