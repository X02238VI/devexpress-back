package com.maw.emd.controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maw.emd.ExcepcionReturnsExpress;
import com.maw.emd.beans.Returns;
import com.maw.emd.beans.requestDevex;
import com.maw.emd.model.ReturnExpressRegistry;
import com.maw.emd.service.ReturnService;
import com.maw.emd.util.ReturnsCodeBar;

@Controller
public class ReturnsController {
	
	@Value("${soap.url}")
	private String url;
	
	@Value("${soap.authorization}")
	private String authorization;
	
	@Value("${email.url}")
	private String urlEmailSend;
	
	
	@Autowired
	private ReturnService returnService;
	
	@GetMapping("barcode/{code}")
    @ResponseBody
    public Returns returnsOrderMain(@PathVariable(name="code", required=true) String code) throws ParseException, ExcepcionReturnsExpress {
		final ReturnsCodeBar codeBar = ReturnsCodeBar.obtenerInstancia();
		@SuppressWarnings("rawtypes")
		final Map datos = codeBar.descodificarCodigoBarrasXtn(code);
		String responseId = new String("400");
		String response = new String("Error Interno");
		int serviceResponse = 0;
		boolean b = Boolean.valueOf((datos.get("esCodigoValido")).toString());
		System.out.println("SOAP URL ENTORNO: " + url);
		System.out.println("Authorization: " + authorization);
		if ( b ) {
			final String tienda = new String(((String) datos.get("codEmpresa")).concat(((String) datos.get("codCentro"))));
			final String operation = new String(((String) datos.get("codTerminal")).concat(((String) datos.get("codTransaccion"))));
			final String pattern = "yyyyMMdd";
			final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			
			if("5".equals((datos.get("identificador")).toString())) {
				responseId = "403";
				response = "Error tarjeta regalo";
		    }else {
		    	String date = simpleDateFormat.format(datos.get("fecha"));
		    	serviceResponse = callService(tienda,date,operation);
		    	if(1 == serviceResponse) {
		    		responseId = "200";
		    		response = (String) (tienda.concat(operation)).concat(date);
		    	}else if(3 == serviceResponse) {
		    		responseId = "401";
					response = "Solo tarjetas de El Corte Ingles";
		    	}else {
		    		responseId = "401";
					response = "No es compra con tarjeta de El Corte Ingles";
		    	}
		    	
		    }
		}else{
			responseId = "402";
			response = "Error Codigo no valido";
		}
			
    	return new Returns(responseId, response);
	}
	


	private int callService(String tienda, String date, String operation) {
		int response = 0;
		try {
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			 con.setRequestMethod("POST");
			 con.setRequestProperty("Authorization", authorization);
			 con.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
		 
			 String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:con=\"http://elcorteingles.es/A6DM/ConsultaOperacion\">\r\n" + 
			 		"   <soapenv:Header/>\r\n" + 
			 		"   <soapenv:Body>\r\n" + 
			 		"      <con:CONSULTA_WS_TICKET_OUT_MT>\r\n" + 
			 		"         <I_TIENDA>"+tienda+"</I_TIENDA>\r\n" + 
			 		"         <I_FECHA>"+date+"</I_FECHA>\r\n" + 
			 		"         <I_HORA></I_HORA>\r\n" + 
			 		"         <I_OPERACION>"+operation+"</I_OPERACION>\r\n" + 
			 		"         <RETURN></RETURN>\r\n" + 
			 		"      </con:CONSULTA_WS_TICKET_OUT_MT>\r\n" + 
			 		"   </soapenv:Body>\r\n" + 
			 		"</soapenv:Envelope>";
			 
			 con.setDoOutput(true);
			 DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			 wr.writeBytes(xml);
			 wr.flush();
			 wr.close();
			 
			 String responseStatus = con.getResponseMessage();
			 System.out.println(responseStatus);
			 BufferedReader in = new BufferedReader(new InputStreamReader(
			 con.getInputStream()));
			 String inputLine;
			 StringBuffer responseSoap = new StringBuffer();
			 
			 while ((inputLine = in.readLine()) != null) {
				 responseSoap.append(inputLine);
				 if(inputLine.contains("CARDNUMBER&#62;000000600833")) {
					 System.out.println("ECI CARD");
					 response = 1; 
				 }
			 }
			 
			 in.close();
			 System.out.println("response:" + responseSoap.toString());
			 //return response.toString();
			 
		 } catch (Exception e) {
			 System.out.println(e);
			 response = 3;
		 }
		return response;
	}

	@PostMapping(path = "data/{code}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Returns returnsOrderData(@PathVariable(name="code", required=true) String code, @RequestBody requestDevex requestdevex) throws Exception {
		String responseId = new String("200");
		String response = new String("");
		
		Integer result = returnService.insertReturn(code, requestdevex);
		if (result == 1) {
		   response = new String("Solicitud Registrada Correctamente.");
		}else {
			responseId = new String("400");
			response = new String("Error al registrar la solicitud, consulte con servicio al Cliente");
		}
		return new Returns(responseId, response);
    }
	
	@PostMapping(path = "notification/{OV}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Returns notificationByOV(@PathVariable(name="OV", required=true) String ov) throws Exception {
		String responseId = new String("200");
		String response = new String("");
		Integer returnsID = new Integer(0);
		String subOv = new String(ov.substring(0,23));
		
		ReturnExpressRegistry returnSelectByOv = returnService.selectByOv(subOv);
		
		if (returnSelectByOv != null) {
			
			String remitente= "attcliente@elcorteingles.info";
			String subject = "Servicio de devoluciones Express del Corte Ingles.";
			String destinatario = returnSelectByOv.getCustomerEmail();
			String body = "Tu devolución ha sido aceptada, en breve recibirás un abono a la tarjeta  con la que pagaste tu compra. Gracias por utilizar nuestro servicio de devolución express.";
			String mensaje = "{\"processNode\":\"default\",\"messageId\":\"980c9b382c5d11e8a5ab0afc45c80000\",";
			mensaje += "\"jodId\":\"\",\"messageType\":\"\",\"priority\":\"\",";
			mensaje +="\"startDate\":\"2018-09-19\",\"startTime\":\"13:10:07.581\",";
			mensaje +="\"endDate\":\"2018-09-19\",\"endTime\":\"13:10:07.581\",";
			mensaje +="\"fromAddress\":\" " + remitente +" \",";
			mensaje +="\"replyAddress\":\"\",\"acknowledgment\":false,";
			mensaje+="\"replyTo\":\"\",\"subject\":\" "+subject+" \",";
			mensaje+="\"templateName\":\"default_email\",";
			mensaje+="\"templateData\":[{\"fieldName\":\"EMAIL\",\"fieldValue\":\""+body+"\"}],";
			mensaje+="\"to\":[\""+destinatario+"\"],";
			mensaje+="\"cc\":[],\"bcc\":[],\"attachments\":[{\"filename\":\"\",\"fileURL\":\"\",\"fileType\":\"\"}]}";	
			
			returnsID = sendMail(mensaje);
			
			if(returnsID == 0 ) {
			    returnsID = returnService.updateStatusByOv(ov, "notificated", "WMB");
		        response = new String("Email Enviado Correctamente.");
			}else {
				responseId = new String("401");
				response   = new String("Error al enviar el email.");
			}
		}else {
			responseId = new String("501");
			response   = new String("Error Interno del servicio");
		}
		return new Returns(responseId, response);
    }
	
	@GetMapping("health")
    @ResponseBody
    public Returns health() throws ParseException, ExcepcionReturnsExpress {
		final String responseId = new String("200");
		final String response = new String("Retunrs Express Service Ok.");
	    return new Returns(responseId, response);
    }
	
	@RequestMapping("/index")
    public String loginMessage(){
        return "index";
    }
	
	@RequestMapping("/wizard")
    public String wizard(){
        return "wizard";
    }
	
	@RequestMapping("/soloTicket")
    public String soloTicket(){
        return "SoloTicket";
    }

    @RequestMapping("/dosTickets")
    public String dosTickets(){
        return "dosTickets";
    }

    @RequestMapping("/unTicket")
    public String unTicket(){
        return "unTicket";
    }
	
	/**
	 * Envía el email, si ha ido bien devuelve 1 sino 0
	 * @return
	 */
	public int sendMail(String mensaje) {
        int retorno = 0;

        try {
            String jsonEmail = mensaje;
            URL url = new URL(urlEmailSend);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            bw.write(jsonEmail);
            bw.flush();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                retorno = 1;
            }
            bw.close();
            InputStream is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder out = new StringBuilder();
            String line;
            while (null != (line = reader.readLine())) {
                out.append(line);
            }
        } catch (MalformedURLException e) {
            retorno = 1;
            System.out.println("MalformedURLException " + e.getStackTrace().toString());
        } catch (IOException e) {
            retorno = 1;
            System.out.println("IOException " + e);
        } catch (Exception e) {
            retorno = 1;
            System.out.println("Exception " + e);
        }
        if(retorno == 0) {
            System.out.println("Se ha enviado el mail");
        }
        return retorno;
    }
	
}
