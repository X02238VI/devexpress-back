package com.maw.emd.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maw.emd.beans.requestDevex;
import com.maw.emd.dao.ReturnExpressRegistryMapper;
import com.maw.emd.model.ReturnExpressRegistry;
import com.maw.emd.model.ReturnExpressRegistryExample;

@Service
public class ReturnService {
	 
	    @Autowired
	    ReturnExpressRegistryMapper returnExpressRegistryMapper;
	 
	    public ReturnExpressRegistry select(BigDecimal code) throws Exception {
	    	ReturnExpressRegistry response = returnExpressRegistryMapper.selectByPrimaryKey(code);
			return response;
	    }
	    
	    public ReturnExpressRegistry selectByOv(String ov) throws Exception {
	    	ReturnExpressRegistryExample example = new ReturnExpressRegistryExample();
	    	example.createCriteria().andOrderNumberEqualTo(ov).andStatusIdEqualTo("created");
			List<ReturnExpressRegistry> response = returnExpressRegistryMapper.selectByExample(example );
			return response.get(0);
	    }
	    
	    public Integer insert(String barCode) throws Exception {
	    	ReturnExpressRegistry record = new ReturnExpressRegistry();
	    		    	
	    	record.setBarCode(barCode);
	    	record.setOrderNumber("1234567890");
	    	record.setCustomerEmail("a@a.com");
	    	record.setCreateIp("127.0.0.1");
	    	record.setCustomerNumber("672079261");
	    	record.setStatusId("created");
	    	record.setCreateDate(new Date());
	    	record.setCreateUser("SYSTEM");
	    	record.setCustomerName("JALP");
	    
	    	Integer response = returnExpressRegistryMapper.insert(record);
			return response;

	    }
	    
	    public Integer insertReturn(String barCode, requestDevex requestdevex) throws Exception {
	    	ReturnExpressRegistry record = new ReturnExpressRegistry();
	    		    	
	    	record.setBarCode(barCode);
	    	record.setOrderNumber(requestdevex.getCodOperacion());
	    	record.setCustomerEmail(requestdevex.getEmail());
	    	record.setCreateIp(requestdevex.getIp());
	    	record.setCustomerNumber(requestdevex.getNumber());
	    	record.setStatusId("created");
	    	record.setCreateDate(new Date());
	    	record.setCreateUser("SYSTEM");
	    	record.setCustomerName("N/A");
			Integer response = returnExpressRegistryMapper.insertSelective(record);
			return response;

	    }
	    
	    public Integer updateStatus(String barcode, String Status) throws Exception {
	    	ReturnExpressRegistry record = new ReturnExpressRegistry();
	    		    	
	    	record.setStatusId(Status);
	    	record.setUpdateDate(new Date());
	    	record.setUpdateUser("WMB");
	    
	    	ReturnExpressRegistryExample example = new ReturnExpressRegistryExample();
	    	example.createCriteria().andBarCodeEqualTo(barcode);
			Integer response = returnExpressRegistryMapper.updateByExampleSelective(record, example );
			return response;

	    }
	    
	    public Integer updateStatusByOv(String Ov, String Status, String userUpdate) throws Exception {
	    	ReturnExpressRegistry record = new ReturnExpressRegistry();
	    		    	
	    	record.setStatusId(Status);
	    	record.setUpdateDate(new Date());
	    	record.setUpdateUser(userUpdate);
	    
	    	ReturnExpressRegistryExample example = new ReturnExpressRegistryExample();
	    	example.createCriteria().andOrderNumberEqualTo(Ov).andStatusIdEqualTo("created");
			Integer response = returnExpressRegistryMapper.updateByExampleSelective(record, example);
			return response;

	    }

}
