package com.maw.emd.model;

import java.math.BigDecimal;
import java.util.Date;

public class ReturnExpressRegistry {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private BigDecimal id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.BAR_CODE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String barCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STATUS_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String statusId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ORDER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String orderNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NAME
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String customerName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_EMAIL
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String customerEmail;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String customerNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String idUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String transactionType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.COMPANY_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String companyId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STORE_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String storeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TERMINAL_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String terminalId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String transactionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String transactionDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.OPERATION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String operationType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.DEPARTMENT_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String departmentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.PARTNERID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String partnerid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_IP
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String createIp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private Date createDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private Date updateDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String createUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String updateUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.LAST_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private String lastMessage;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.POSTDM_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    private byte[] postdmMessage;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID
     *
     * @param id the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.BAR_CODE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.BAR_CODE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getBarCode() {
        return barCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.BAR_CODE
     *
     * @param barCode the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.BAR_CODE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? null : barCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STATUS_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STATUS_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getStatusId() {
        return statusId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STATUS_ID
     *
     * @param statusId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STATUS_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setStatusId(String statusId) {
        this.statusId = statusId == null ? null : statusId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ORDER_NUMBER
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ORDER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ORDER_NUMBER
     *
     * @param orderNumber the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ORDER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber == null ? null : orderNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NAME
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NAME
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NAME
     *
     * @param customerName the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NAME
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_EMAIL
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_EMAIL
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_EMAIL
     *
     * @param customerEmail the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_EMAIL
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail == null ? null : customerEmail.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NUMBER
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NUMBER
     *
     * @param customerNumber the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CUSTOMER_NUMBER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber == null ? null : customerNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID_USER
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID_USER
     *
     * @param idUser the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.ID_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser == null ? null : idUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_TYPE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_TYPE
     *
     * @param transactionType the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType == null ? null : transactionType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.COMPANY_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.COMPANY_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.COMPANY_ID
     *
     * @param companyId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.COMPANY_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STORE_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STORE_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STORE_ID
     *
     * @param storeId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.STORE_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId == null ? null : storeId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TERMINAL_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TERMINAL_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TERMINAL_ID
     *
     * @param terminalId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TERMINAL_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId == null ? null : terminalId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_ID
     *
     * @param transactionId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId == null ? null : transactionId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_DATE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_DATE
     *
     * @param transactionDate the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.TRANSACTION_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate == null ? null : transactionDate.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.OPERATION_TYPE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.OPERATION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getOperationType() {
        return operationType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.OPERATION_TYPE
     *
     * @param operationType the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.OPERATION_TYPE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setOperationType(String operationType) {
        this.operationType = operationType == null ? null : operationType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.DEPARTMENT_ID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.DEPARTMENT_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getDepartmentId() {
        return departmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.DEPARTMENT_ID
     *
     * @param departmentId the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.DEPARTMENT_ID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId == null ? null : departmentId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.PARTNERID
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.PARTNERID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getPartnerid() {
        return partnerid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.PARTNERID
     *
     * @param partnerid the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.PARTNERID
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid == null ? null : partnerid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_IP
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_IP
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCreateIp() {
        return createIp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_IP
     *
     * @param createIp the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_IP
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCreateIp(String createIp) {
        this.createIp = createIp == null ? null : createIp.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_DATE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_DATE
     *
     * @param createDate the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_DATE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_DATE
     *
     * @param updateDate the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_DATE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_USER
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_USER
     *
     * @param createUser the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.CREATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_USER
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_USER
     *
     * @param updateUser the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.UPDATE_USER
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.LAST_MESSAGE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.LAST_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.LAST_MESSAGE
     *
     * @param lastMessage the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.LAST_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage == null ? null : lastMessage.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.POSTDM_MESSAGE
     *
     * @return the value of DEVEXPRESS.RETURN_EXPRESS_REGISTRY.POSTDM_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public byte[] getPostdmMessage() {
        return postdmMessage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DEVEXPRESS.RETURN_EXPRESS_REGISTRY.POSTDM_MESSAGE
     *
     * @param postdmMessage the value for DEVEXPRESS.RETURN_EXPRESS_REGISTRY.POSTDM_MESSAGE
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    public void setPostdmMessage(byte[] postdmMessage) {
        this.postdmMessage = postdmMessage;
    }
}