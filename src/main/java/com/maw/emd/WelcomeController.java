package com.maw.emd;

public class WelcomeController extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	public WelcomeController(String msg) {
        super(msg);
    }
	
}
