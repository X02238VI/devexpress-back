package com.maw.emd.util;

public interface IReturnsCodeBar {
  	public static final String COD_VALIDO = "esCodigoValido";
    public static final String IDENTIFICADOR = "identificador";
    public static final String COD_VENDEDOR = "codVendedor";
    public static final String COD_CENTRO = "codCentro";
    public static final String TIP_TRANSACCION = "tipoTransaccion";
    public static final String COD_EMPRESA = "codEmpresa";
    public static final String COD_TERMINAL = "codTerminal";
    public static final String COD_TRANSACCION = "codTransaccion";
    public static final String FECHA = "fecha";
    public static final String TIP_OPERACION = "tipoOperacion";
}
