package com.maw.emd.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class ReturnsCodeBar
    implements IReturnsCodeBar
{

    private ReturnsCodeBar()
    {
        longSubcadenaNoCodificada = 9;
        longCodigoBarrasXtn = 23;
        longCadenaNoCodificada = 34;
        longSubcadenaCodBarras = 6;
        formatoFechaHora = new SimpleDateFormat("ddMMyy");
    }

    public static ReturnsCodeBar obtenerInstancia()
    {
        if(instancia == null)
            instancia = new ReturnsCodeBar();
        return instancia;
    }

    public Map descodificarCodigoBarrasXtn(String codBarrasTransaccion)
        throws ParseException
    {
        Map datos = new HashMap();
        if(codBarrasTransaccion.length() != longCodigoBarrasXtn)
        {
            datos.put("esCodigoValido", Boolean.FALSE);
            return datos;
        } else
        {
            StringBuilder cadenaDescodificada = new StringBuilder(longCadenaNoCodificada);
            
            System.out.println("cadenaDescodificada:"+cadenaDescodificada);
            
            descodificar(codBarrasTransaccion, cadenaDescodificada, longCodigoBarrasXtn);
            String digitoControl = digitoControl(cadenaDescodificada);
            String digitoControlEan = digitoEan(cadenaDescodificada);
            
            System.out.println("Digito EAN:"+digitoControlEan);
            
            String dCCadena = dCCadena(cadenaDescodificada, longCadenaNoCodificada);
            return retornarDatos(digitoControl, digitoControlEan, dCCadena, cadenaDescodificada, datos);
        }
    }

    private StringBuilder bADecimal(String entrada)
    {
        String temp = Integer.valueOf(entrada, 36).toString();
        StringBuilder salida = new StringBuilder();
        salida.append(relleno, 0, longSubcadenaNoCodificada);
        salida.replace(9 - temp.length(), 9, temp);
        return salida;
    }

    private void descodificar(String codBarrasTransaccion, StringBuilder cadenaDescodificada, int longCodigoBarrasXtn)
    {
        for(int posInicio = 0; posInicio < longCodigoBarrasXtn; posInicio += longSubcadenaCodBarras)
        {
            int posFinalSubcadena = posInicio + longSubcadenaCodBarras;
            if(posFinalSubcadena > longCodigoBarrasXtn)
            {
                String subCadena = codBarrasTransaccion.substring(posInicio);
                StringBuilder cadenaNueve = bADecimal(subCadena);
                cadenaDescodificada.append(cadenaNueve.substring(2));
            } else
            {
                String subCadena = codBarrasTransaccion.substring(posInicio, posFinalSubcadena);
                cadenaDescodificada.append(bADecimal(subCadena));
            }
        }

    }

    private String digitoControl(StringBuilder cadena)
    {
        return obtenerDC(cadena.substring(0, 33), 2, true);
    }

    private String digitoEan(StringBuilder cadena)
    {
        return obtenerDC(cadena.substring(0, 33), 3, false);
    }

    private String obtenerDC(String strCode, int iPeso, boolean blnSumarDigitos)
    {
        String strDigito = "";
        if(strCode != null && strCode.length() > 0)
        {
            int iWeightI = iPeso;
            int iWeightP = 1;
            int iLength = strCode.length();
            int result = 0;
            int iWeight = iWeightI;
            int iSuma = iWeightP + iWeightI;
            int ipos = 0;
            int iaux = 0;
            String szAux = null;
            int total = 0;
            for(ipos = iLength - 1; ipos >= 0; ipos--)
            {
                iaux = iWeight * (strCode.charAt(ipos) - 48);
                if(blnSumarDigitos && iaux > 9)
                {
                    szAux = Integer.toString(iaux, 10);
                    iaux = (szAux.charAt(0) - 48) + (szAux.charAt(1) - 48);
                }
                total += iaux;
                iWeight = iSuma - iWeight;
            }

            result = 10 - total % 10;
            if(result == 10)
                result = 0;
            strDigito = String.valueOf(result);
        }
        return strDigito;
    }

    private String dCCadena(StringBuilder cadena, int longitud)
    {
        return cadena.substring(longitud - 1, longitud);
    }

    private Map retornarDatos(String digitoControl, String digitoControlEan, String dCCadena, StringBuilder cadena, Map datos)
        throws ParseException
    {
        if(!digitoControl.equals(dCCadena) && !digitoControlEan.equals(dCCadena))
        {
            datos.put("esCodigoValido", Boolean.FALSE);
        } else
        {
        	System.out.println("Cadena:"+cadena);
            datos.put("esCodigoValido", Boolean.TRUE);
            datos.put("identificador", cadena.substring(0, 1));
            String codVendedor = cadena.substring(1, 8);
            codVendedor = codVendedor.concat(obtenerDC(codVendedor, 2, true));
            datos.put("codVendedor", codVendedor);
            datos.put("tipoTransaccion", cadena.substring(8, 10));
            datos.put("codEmpresa", cadena.substring(10, 13));
            datos.put("codCentro", cadena.substring(13, 17));
            datos.put("codTerminal", cadena.substring(17, 21));
            datos.put("codTransaccion", cadena.substring(21, 25));
            datos.put("fecha", formatoFechaHora.parse(cadena.substring(25, 31)));
            datos.put("tipoOperacion", cadena.substring(31, 33));
        }
        return datos;
    }

    private static ReturnsCodeBar instancia;
    private int longSubcadenaNoCodificada;
    private int longCodigoBarrasXtn;
    private int longCadenaNoCodificada;
    private int longSubcadenaCodBarras;
    private char relleno[] = {
        '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
    };
    private SimpleDateFormat formatoFechaHora;
}
