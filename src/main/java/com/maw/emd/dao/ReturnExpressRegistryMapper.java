package com.maw.emd.dao;

import com.maw.emd.model.ReturnExpressRegistry;
import com.maw.emd.model.ReturnExpressRegistryExample;
import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface ReturnExpressRegistryMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int countByExample(ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int deleteByExample(ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int deleteByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int insert(ReturnExpressRegistry record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int insertSelective(ReturnExpressRegistry record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    List<ReturnExpressRegistry> selectByExampleWithBLOBs(ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    List<ReturnExpressRegistry> selectByExample(ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    ReturnExpressRegistry selectByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByExampleSelective(@Param("record") ReturnExpressRegistry record, @Param("example") ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByExampleWithBLOBs(@Param("record") ReturnExpressRegistry record, @Param("example") ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByExample(@Param("record") ReturnExpressRegistry record, @Param("example") ReturnExpressRegistryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByPrimaryKeySelective(ReturnExpressRegistry record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByPrimaryKeyWithBLOBs(ReturnExpressRegistry record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DEVEXPRESS.RETURN_EXPRESS_REGISTRY
     *
     * @mbggenerated Wed Nov 07 15:37:01 CET 2018
     */
    int updateByPrimaryKey(ReturnExpressRegistry record);
}