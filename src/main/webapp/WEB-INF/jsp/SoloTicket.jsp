<!doctype html>
<html lang="es">
	
<head>
	
<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Generador de c&oacutedigo de barras</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" /> -->

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
	<link href="assets/css/ticket.css" rel="stylesheet" />
	
	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	
	<!--  Plugin for the Wizard -->
	<script src="assets/js/material-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

	<!-- CODIGOS DE BARRAS -->
	<!--<script type="text/javascript" src="assets/js/JsBarcode.all.min.js"></script>-->
	<script src="assets/js/jquery-barcode.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery-barcode.js" type="text/javascript"></script>
	<script src="assets/js/qrcode.js" type="text/javascript"></script>
	<script src="assets/js/qrcode.min.js" type="text/javascript"></script>
	
	<style>
		@media print{
			.saltoDePagina{
				display:block;
				page-break-before:always;
			}
		}
	</style>
	
</head>

<body onload="recibirParametros()">
		<center>
		<div class="centrado" id="imprimible" >
			
			<img src="assets/img/supermer.bmp" width="270" height="100" alt="Logotipo">
			<div style="line-height: 1.2;">
		    	
				<h3> <b>DEVOLUCI&OacuteN EXPRESS </b></h3>
	    	</div>
	    	
	    	<!--CODIGO DE BARRAS -->
	    	<center><div class = "centrado" id="barcode"> </div></center>

	    	<div class="centrado" style="line-height: 1.2;">
	    		<br><input type="text" id="fechaHora" style=" width:200px;border:none;">
	        	
	        	<br><input type="text" id="tkEmail" size="42" maxlengt="27" style=" border:none;"/>
	        	<br><input type="text" id="tkEmail2" size="42" maxlengt="27" style="border:none;"/>
	        	
	        	<br><input type="text" id="tkTelefono" style=" width:200px;border:none;"/>
	        	<br><input type="text" id="codOperacion" style=" width:200px;border:none;"/>
	    		
	    		<br>
	    		<center><div id="barcodeqr"> </div></center>
	    		<br>
	    		<p> Muchas gracias, se realizar&aacute el abono lo antes posible y recibir&aacute una comunicaci&oacuten del mismo en su correo electr&oacutenico</p>
			</div>
		
		</div> 
		</center>
		<!--<input type='button' class='btn btn- btn-fill btn-default btn-wd btn-success btn-primary btn-sm' name='reimprimir' value='Re-imprimir' onclick="pruebas()"> -->
		<div class="saltoDePagina"></div>
		<center>
		<div class="centrado" id="imprimible2" >
			
			<img src="assets/img/supermer.bmp" width="270" height="100" alt="Logotipo">
			<div style="line-height: 1.2;">
		    	
				<h3> <b>DEVOLUCI&OacuteN EXPRESS </b></h3>
	    	</div>
	    	
	    	<!--CODIGO DE BARRAS -->
	    	<center><div class = "centrado" id="barcode_"> </div></center>

	    	<div class="centrado" style="line-height: 1.2;">
	    		<br><input type="text" id="fechaHora_" style=" width:200px;border:none;">
	        	<br><input type="text" id="tkEmail_" style=" width:200;border:none;"/>
	        	<br><input type="text" id="tkEmail2_" style=" width:200;border:none;"/>
	        	<br><input type="text" id="tkTelefono_" style=" width:200px;border:none;"/>
	        	<br><input type="text" id="codOperacion_" style=" width:200px;border:none;"/>
	    		
	    		<br>
	    		<center><div id="barcodeqr_"> </div></center>
	    		<br>
	    		<p> Muchas gracias, se realizar&aacute el abono lo antes posible y recibir&aacute una comunicaci&oacuten del mismo en su correo electr&oacutenico</p>
			</div>
		
		</div> 
		</center>
</body>
</html>
