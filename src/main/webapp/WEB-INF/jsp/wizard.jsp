	<!doctype html>
<html lang="es">
	
<head>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<script>
		var urlHTML = "";
		//var urlHTML = "https://8913929f-eafa-4c31-aafa-c5bde9e173c0.mock.pstmn.io/dev-express/";
		//var urlHTML = "";
	</script>
	<title>Servicio de devoluci&oacuten express de El Corte Ingl&eacutes.</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" /> -->

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/material-bootstrap-wizard.css" rel="stylesheet">

	<!-- css para la extensión del conjunto de teclas de vista previa -->
 	<link href = "assets/css/keyboard-previewkeyset.css" rel="stylesheet">
	

	<!--   Core JS Files   -->
	<script src="assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>
	<!--<script src="assets/js/jquery-latest.min.js" type="text/javascript"></script>-->
	
	
	<!--TECLADO -->
	<link href="assets/css/jquery-ui.min.css" rel="stylesheet">
	<!--<script src="assets/js/jquery-latest.min.js"></script>-->
 	<script src = "//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js "> </script>
 	<!-- Tema jQuery UI o Bootstrap (opcional, si crea un tema personalizado) -->
 	<link href = "//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/ui-lightness/jquery-ui.css " rel = " stylesheet ">
	<!-- keyboard widget css & script (required) -->
	<link href="assets/css/keyboard.css" rel="stylesheet">
	<script src="assets/js/jquery.keyboard.js"></script>
	<!-- keyboard extensions (optional) -->
	<script src="assets/js/jquery.keyboard.extension-all.js"></script>
	<script src="assets/js/jquery.mousewheel.js"></script>
	<script src="assets/js/jquery.keyboard.extension-typing.js"></script>
	<script src="assets/js/jquery.keyboard.extension-autocomplete.js"></script>
	<script src="assets/js/jquery.keyboard.extension-caret.js"></script>
	

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

	<!-- CODIGOS DE BARRAS -->
	<!--<script type="text/javascript" src="assets/js/JsBarcode.all.min.js"></script>-->
	<script src="assets/js/jquery-barcode.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery-barcode.js" type="text/javascript"></script>
	<script src="assets/js/qrcode.js" type="text/javascript"></script>

	<!-- POPUP -->
	<script src="assets/js/bootbox.min.js" type="text/javascript"></script>



	<!--  Plugin for the Wizard -->
	<script src="assets/js/material-bootstrap-wizard.js"></script>

	
	
	
	
</head>

<body onload = "VozInstrucciones()">
	<div>
	    

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="green" id="wizard" >
		                    <form action="" method="">
		                <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->

		                    	<div class="wizard-header btn-next">
		                        	<img src="assets/img/tarjetaEci.png" width="600" height="150" >
		                    	</div>
								<div class="wizard-navigation">
									<ul>
										
			                            <li><a href="#instrucciones" data-toggle="tab">Instrucciones</a></li>
			                            <li><a href="#introcodigo" data-toggle="tab">C&oacutedigo de tique</a></li>
			                            <li><a href="#introdatos" data-toggle="tab">Introducir datos</a></li>
			                            <li><a href="#generar" data-toggle="tab">Generaci&oacuten del c&oacutedigo</a></li>
			                        </ul>
								</div>
		
		
			                        <div class="tab-content">
			
			<!--INSTRUCCIONES-->
		                            <div id="instrucciones" class='tab-pane' >
		                            	<div class="col-sm-10 col-sm-offset-1">
		                                        <div class="col-sm-4">
		                                            <div class="choice btn-next" >
		                                                <!--<input type="radio" name="job" value="Design">-->
		                                                <div class="icon">
		                                                    <i class="material-icons">scanner</i>
		                                                </div>
		                                                <h6>Escanea el c&oacutedigo de barras de tu tique de venta e introduce tu email y tel&eacutefono</h6>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice btn-next" >
		                                                <input type="radio" name="job" value="Code">
		                                                <div class="icon">
		                                                    <i class="material-icons">shopping_basket</i>
		                                                </div>
		                                                <h6>Introduce los art&iacuteculos a devolver en una bolsa junto con uno de los dos tiques impresos. Recuerda conservar el otro tique como justificante</h6>
		                                            </div>
		                                        </div>
												<div class="col-sm-4">
		                                            <div class="choice btn-next" >
		                                                <input type="radio" name="job" value="Code">
		                                                <div class="icon">
		                                                    <i class="material-icons">swap_horiz</i>
		                                                </div>
		                                                <h6>Introduce la bolsa en el contenedor y realizaremos la devoluci&oacuten a la mayor brevedad posible tras revisar la mercanc&iacutea</h6>
		                                            </div>
		                                        </div>
		                                        
		                            	</div>	
		                            </div>
		<!--INTRODUCIR CODIGO-->
		                            <div class="tab-pane" id="introcodigo">
		                            	<br>
		                            	<br>
		                            	
		                            	
		                            	<center>
										     <div class="logo">
								                <img src="assets/img/manawa.png" sizes="76x76" width="300" height="150">
								            </div>
										</center>
										
		                            	
		                            	
		                            	<div class="col-sm-3">
		                            	</div>
		                            	<center>
			                            	<div class="col-sm-6">
			                            		<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">scanner</i>
														
													</span>
													<div class="form-group label-floating">
														<label class="control-label">Escanea el c&oacutedigo:</label>
			                                          	<input autofocus type="text" class="form-control" 
			                            		   size="30" value="" name="barcodeValue" id="barcodeValue" onchange="generarCodigo()">
			                            		   		<span>
				                                        	<i class="material-icons">
															arrow_forward 
															</i>
														</span>
			                            		   		<i class="material-icons hiddenInputCodigo" type="button">keyboard</i>
			                            		   		<span>
				                                        <i class="material-icons">
															arrow_back 
															</i>
														</span>

			                            		        <input id="hiddenCodigo" type="text" style="display:none;" onchange="copiarCodigo()">
			                            		        
			                            		        
			                                        </div>
			                                        
			                                        <!--<label class="control-label">(Si no puede leer el código, utilice el teclado)</label>
			                                        (Si no puede escanear el código correctamente puede utilizar el teclado para introducirlo)-->
			                                        
												</div>
			                            	</div>	
		                            	</center>
		                                
		                            </div>
		<!--INTRODUCIR DATOS-->
		                            <div class="tab-pane" id="introdatos">
		                            	
		                            	<br>
		                            	<br>
		                            	
		                            	
		                            	<center>
										     <div class="logo">
								                <img src="assets/img/manawa_.png" sizes="76x76" width="200" height="150">
								            </div>
										</center>
										
		                            	
		                            	<div >
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">email</i>
												</span>
												<div class="form-group label-floating">
		                                          	<label class="control-label">Tu Email</label>
		                                          	<input name="email" type="text" id="email" class="form-control hiddenInputEmail">
		                                          	<input id="hiddenEmail" type="text" style="display:none;" onchange="copiarEmail()">
		                                        </div>
											</div>

											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">phone</i>
												</span>
												<div class="form-group label-floating">
		                                          	<label class="control-label">Tu Tel&eacutefono</label>
		                                          	<input name="telefono" id="telefono" type="text" class="form-control hiddenInputTfo">
		                                          	<input id="hiddenTfo" type="text" style="display:none;" onchange="copiarTfo()">
		                                        </div>
											</div>

	                                	</div>
		                            	
		                                
		                            </div>
		
		<!--GENERAR-->
		                            <div class="tab-pane" id="generar">
		                                <div class="row">
		                                    
		                                    
	                                    	<!--<div class="col-md-1" >
	                                    	</div>-->
											<div class="ticket col-sm-5" id="ticket">
													<div id="mensaje" style="width:300px; border-width: thin;
																	    border-style: solid;
																	    border-color: #000;">
														<div><b> PASO 1: </b></div>
														
														<p name = "msgFinal" id = "msgFinal" style="margin:7px;border-width: thin;
																	    border-style: solid;
																	    border-color: #000;">
															<b>Recuerda: debes introducir uno de los dos tiques impresos en la bolsa junto con la mercanc&iacutea. Guarda el otro tique junto con el tique de venta como justificante de la operaci&oacuten.</b>
														</p>
														
														
												    	<div id="imprimible"  style="margin:7px;width:280px; border-width: thin;
																		    border-style: solid;
																		    border-color: #000;">
												    		<!--<img src="assets/img/ecipequeno.png" sizes="30x30" alt="Logotipo">-->
															<img src="assets/img/supermer.bmp" width="270" height="100" alt="Logotipo">
															<div style="line-height: 1.2;">
														    	
																<center><h4> <b>DEVOLUCI&OacuteN EXPRESS </b></h4></center>
													    	</div>
													    	
													    	<!--CODIGO DE BARRAS -->
													    	<center><div style="width: 100px" id="barcode"> </div></center>
													    	
													    	

													    	<div style="line-height: 1.2;display:none;">
													    		<br><input type="text" id="fechaHora" style="width:200px; border:none;">
													        	<br><input type="text" id="tkEmail" style="width:270px; border:none;"/>
													        	<br><input type="text" id="tkTelefono" style=" border:none;"/>
													        	<br><input type="text" id="codOperacion" style=" border:none;"/>
													    		</p>
															</div>
		                                    			</div>

													</div>
		                                    	
		                                	</div>
		                                	<div class="col-md-1">
	                                    	</div>
		                                	<div class="ticket col-sm-5" id="reimprimir">
												<div id="mensaje" style="width:300px; border-width: thin;
																    border-style: solid;
																    border-color: #000;">
													
													<div><b> PASO 2: </b></div>
													<p name = "msgReimprimir" id = "msgReimprimir" style="width:280px;margin:7px;border-width: thin;
																    border-style: solid;
																    border-color: #000;">
														<b>Si vas a devolver m&aacutes de un art&iacuteculo, introduce cada art&iacuteculo en una bolsa. Reimprime tantos tiques como art&iacuteculos devueltos.</b>
													</p>
													<br>
													<br>
													<br>
													<br>
													<br>
													<br>
													<br>
													<center>
			                                    	<div class="centrado">
		                                    			<input type='button' class='btn btn-fill btn-default btn-wd btn-success btn-primary btn-md' name='reimprimir' value='Re-imprimir' onclick="imprimirUnTicket()">
		                                    		</div>
				                                	</center>	
				                                	<br>
													<br>
												</div>
												
		                                	</div>
		                                </div>
		                            </div>

		                        </div>
		                        
	                        	<div class="wizard-footer">
	                            	<div class="pull-right">
	                                    <input type='button' class='btn btn-next btn-fill btn-wd btn-success' name='next' id='next' value='Siguiente'>
	                                    
	                                    <input type='button' class='btn btn-finish btn-fill btn-wd btn-success' name='finish' value='Terminar' onclick="terminar()">

	                                </div>
	                                <div class="pull-left">
	                                	
	                                    <input type='button' class='btn btn-centrado  btn-wd btn-success' name='exit' id="exit" value='Salir' onclick="terminar()">
	                                </div>    
	                                <div class="pull-left">
	                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-success' name='previous' value='Anterior'>

										
	                                </div>
	                                <div class="clearfix"></div>
	                        	</div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	    	</div> <!-- row -->
		</div> <!--  big container -->

	    
	</div>

	<!-- POPUP -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel"></h4>
	      </div>
	      <div class="modal-body" id="msgModal">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
	      </div>
	    </div>
	  </div>
	</div>

</body>
	

	
</html>
