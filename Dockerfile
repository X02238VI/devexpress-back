FROM dcos-registry.pre.eci.geci:5000/eci/centos:7
MAINTAINER Manawa Consulting, emd@manawac.com
#VOLUME /devexpress-back

ENV http_proxy "##HTTP_PROXY##"
ENV https_proxy "##HTTP_PROXY##"
ENV JAVA_OPTS="-classpath /devexpress-back/cfg:/devexpress-back/lib:/devexpress-back/wsdl"
ENV JAVA_HOME /opt/java-jdk-1.8
ENV PATH $PATH:$JAVA_HOME/bin

RUN mkdir -p /devexpress-back/lib && \
    mkdir -p /devexpress-back/log && \
    mkdir -p /devexpress-back/cfg && \
    mkdir -p /devexpress-back/db && \
    mkdir -p /devexpress-back/wsdl && \
    chmod -R 777 /devexpress-back

RUN yum install -y unzip wget
#RUN alternatives --install /usr/bin/java jar /usr/java/latest/bin/java 200000 && \
#    alternatives --install /usr/bin/javaws javaws /usr/java/latest/bin/javaws 200000 && \
#    alternatives --install /usr/bin/javac javac /usr/java/latest/bin/javac 200000

# Using CI process
RUN curl http://nexus.elcorteingles.int/service/local/repositories/GC/content/es/eci/ic/nexusDownloader/1.0.1.0/nexusDownloader-1.0.1.0.zip > nexusDownloader-1.0.1.0.zip && \
unzip nexusDownloader-1.0.1.0.zip && rm -f nexusDownloader-1.0.1.0.zip && chmod +x nexus_downloader.sh
RUN ./nexus_downloader.sh com.maw.emd.returns.express devexpress-back ##BUILD_VERSION## zip
RUN unzip devexpress-back-*zip -d /
RUN mv /devexpress-back/devexpress-back*.war /devexpress-back/devexpress-back.war

# JDK 1.8 - Para compilar
RUN mkdir -p /opt/java-jdk-1.8 && \
    chmod 755 /opt/java-jdk-1.8 && \
    wget --proxy=off  http://nexus.elcorteingles.int/service/local/repositories/3rd/content/com/ibm/java_jdk/1.8/java_jdk-1.8.zip -P /tmp/ -nv && \
    unzip /tmp/java_jdk-1.8.zip -d /opt/java-jdk-1.8 && \
    rm -f /tmp/java_jdk-1.8.zip
WORKDIR /devexpress-back
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar devexpress-back.jar" ]
