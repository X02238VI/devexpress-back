CREATE OR REPLACE PROCEDURE deleteKiosko(creacion IN VARCHAR2, 
								   modificacion IN VARCHAR2, 
								   estado IN RETURN_EXPRESS_REGISTRY.STATUS_ID%TYPE,
								   commit_after_in IN PLS_INTEGER)
IS

registry_cursor SYS_REFCURSOR; 
sql_text        varchar2(1000); 
rec             RETURN_EXPRESS_REGISTRY.id%TYPE;
l_counter       PLS_INTEGER;
BEGIN
   l_counter := 1;
   
   sql_text := 'SELECT id FROM RETURN_EXPRESS_REGISTRY WHERE ';
    IF creacion IS NOT NULL THEN
        sql_text := sql_text || 'CREATE_DATE < TO_DATE('''|| creacion ||''', ''YYYY/MM/DD'')';
        END IF;
    IF creacion IS NOT NULL AND modificacion IS NOT NULL THEN
        sql_text := sql_text || ' and UPDATE_DATE < TO_DATE('''|| modificacion ||''', ''YYYY/MM/DD'')';
        END IF;
    IF creacion IS NULL AND modificacion IS NOT NULL THEN
        sql_text := sql_text || 'UPDATE_DATE < TO_DATE('''|| modificacion ||''', ''YYYY/MM/DD'')';
        END IF;
    IF (creacion IS NOT NULL OR modificacion IS NOT NULL) AND estado IS NOT NULL THEN
        sql_text := sql_text || ' and STATUS_ID=' || estado;
        END IF;
    IF creacion IS NULL AND modificacion IS NULL AND estado IS NOT NULL THEN
        sql_text := sql_text || 'STATUS_ID=' || estado;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE(sql_text);

    OPEN registry_cursor FOR sql_text; 
      
    LOOP  
      FETCH registry_cursor INTO rec;
      EXIT WHEN registry_cursor%NOTFOUND;
        DELETE RETURN_EXPRESS_REGISTRY WHERE ID=rec;
        IF l_counter >= commit_after_in  
        THEN  
           COMMIT;  
           l_counter := 1;  
        ELSE  
           l_counter := l_counter + 1;  
        END IF;  
    END LOOP;
    CLOSE registry_cursor;
    COMMIT; 
    DBMS_OUTPUT.PUT_LINE('Final');
END;



